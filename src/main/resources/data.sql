-- regions
insert into regions (name) values ('kanto');
insert into regions (name) values ('sevii islands');
insert into regions (name) values ('johto');
insert into regions (name) values ('hoenn');
insert into regions (name) values ('sinnoh');
insert into regions (name) values ('unova');
insert into regions (name) values ('kalos');
insert into regions (name) values ('alola');

-- areas / routes
insert into areas (name) values ('route 1');
insert into areas (name) values ('route 2');
insert into areas (name) values ('route 3');
insert into areas (name) values ('route 4');
insert into areas (name) values ('route 5');
insert into areas (name) values ('safari zone');

-- types
insert into pokemon_types (name) values ('fire');     -- 1
insert into pokemon_types (name) values ('water');    -- 2
insert into pokemon_types (name) values ('grass');    -- 3
insert into pokemon_types (name) values ('electric'); -- 4
insert into pokemon_types (name) values ('ground');   -- 5
insert into pokemon_types (name) values ('rock');     -- 6
insert into pokemon_types (name) values ('ice');      -- 7
insert into pokemon_types (name) values ('dragon');   -- 8
insert into pokemon_types (name) values ('ghost');    -- 9
insert into pokemon_types (name) values ('psychic');  -- 10
insert into pokemon_types (name) values ('dark');     -- 11
insert into pokemon_types (name) values ('fairy');    -- 12
insert into pokemon_types (name) values ('normal');   -- 13
insert into pokemon_types (name) values ('flying');   -- 14
insert into pokemon_types (name) values ('bug');      -- 15
insert into pokemon_types (name) values ('steel');    -- 16
insert into pokemon_types (name) values ('fight');    -- 17
insert into pokemon_types (name) values ('poison');   -- 18

-- type weaknesses
insert into weaknesses (type_strong_id,type_weak_id) values (1,3);
insert into weaknesses (type_strong_id,type_weak_id) values (1,7);
insert into weaknesses (type_strong_id,type_weak_id) values (1,15);
insert into weaknesses (type_strong_id,type_weak_id) values (1,16);
insert into weaknesses (type_strong_id,type_weak_id) values (2,1);
insert into weaknesses (type_strong_id,type_weak_id) values (2,5);
insert into weaknesses (type_strong_id,type_weak_id) values (2,6);
insert into weaknesses (type_strong_id,type_weak_id) values (3,5);
insert into weaknesses (type_strong_id,type_weak_id) values (3,6);
insert into weaknesses (type_strong_id,type_weak_id) values (3,2);
insert into weaknesses (type_strong_id,type_weak_id) values (4,2);
insert into weaknesses (type_strong_id,type_weak_id) values (4,14);
insert into weaknesses (type_strong_id,type_weak_id) values (5,1);
insert into weaknesses (type_strong_id,type_weak_id) values (5,4);
insert into weaknesses (type_strong_id,type_weak_id) values (5,18);
insert into weaknesses (type_strong_id,type_weak_id) values (5,6);
insert into weaknesses (type_strong_id,type_weak_id) values (5,16);
insert into weaknesses (type_strong_id,type_weak_id) values (6,1);
insert into weaknesses (type_strong_id,type_weak_id) values (6,7);
insert into weaknesses (type_strong_id,type_weak_id) values (6,14);
insert into weaknesses (type_strong_id,type_weak_id) values (6,15);
insert into weaknesses (type_strong_id,type_weak_id) values (7,3);
insert into weaknesses (type_strong_id,type_weak_id) values (7,5);
insert into weaknesses (type_strong_id,type_weak_id) values (7,14);
insert into weaknesses (type_strong_id,type_weak_id) values (7,8);
insert into weaknesses (type_strong_id,type_weak_id) values (8,8);
insert into weaknesses (type_strong_id,type_weak_id) values (9,10);
insert into weaknesses (type_strong_id,type_weak_id) values (9,9);
insert into weaknesses (type_strong_id,type_weak_id) values (10,17);
insert into weaknesses (type_strong_id,type_weak_id) values (10,18);
insert into weaknesses (type_strong_id,type_weak_id) values (11,10);
insert into weaknesses (type_strong_id,type_weak_id) values (11,9);
insert into weaknesses (type_strong_id,type_weak_id) values (12,17);
insert into weaknesses (type_strong_id,type_weak_id) values (12,8);
insert into weaknesses (type_strong_id,type_weak_id) values (12,11);
insert into weaknesses (type_strong_id,type_weak_id) values (14,3);
insert into weaknesses (type_strong_id,type_weak_id) values (14,17);
insert into weaknesses (type_strong_id,type_weak_id) values (14,15);
insert into weaknesses (type_strong_id,type_weak_id) values (15,3);
insert into weaknesses (type_strong_id,type_weak_id) values (15,10);
insert into weaknesses (type_strong_id,type_weak_id) values (15,11);
insert into weaknesses (type_strong_id,type_weak_id) values (16,7);
insert into weaknesses (type_strong_id,type_weak_id) values (16,6);
insert into weaknesses (type_strong_id,type_weak_id) values (16,12);
insert into weaknesses (type_strong_id,type_weak_id) values (17,13);
insert into weaknesses (type_strong_id,type_weak_id) values (17,7);
insert into weaknesses (type_strong_id,type_weak_id) values (17,6);
insert into weaknesses (type_strong_id,type_weak_id) values (17,11);
insert into weaknesses (type_strong_id,type_weak_id) values (17,16);
insert into weaknesses (type_strong_id,type_weak_id) values (18,3);
insert into weaknesses (type_strong_id,type_weak_id) values (18,12);

-- pokemonMovements
insert into movements (name, description) values ('Growl','The user growls in an endearing way, making opposing Pokémon less wary. This lowers their Attack stat.');
insert into movements (name, description) values ('Scratch','Hard, pointed, sharp claws rake the target to inflict damage.');
insert into movements (name, description) values ('Tackle','A physical attack in which the user charges and slams into the target with its whole body.');
insert into movements (name, description) values ('Tail Whip','The user wags its tail cutely, making opposing Pokémon less wary and lowering their Defense stat.');

-- kanto starters
insert into pokemon (name, description) values ('bulbasaur','the grass pokemon');
insert into pokemon_pokemon_types (pokemon_id,pokemon_type_id) values (1,3);
insert into pokemon_pokemon_types (pokemon_id,pokemon_type_id) values (1,18);
insert into pokemon (name, description) values ('squirtle','the water pokemon');
insert into pokemon_pokemon_types (pokemon_id,pokemon_type_id) values (2,2);
insert into pokemon (name, description) values ('charmander','the fire pokemon');
insert into pokemon_pokemon_types (pokemon_id,pokemon_type_id) values (3,1);
insert into pokemon_regions (pokemon_id,region_id) values (1,1);
insert into pokemon_regions (pokemon_id,region_id) values (2,1);
insert into pokemon_regions (pokemon_id,region_id) values (3,1);
